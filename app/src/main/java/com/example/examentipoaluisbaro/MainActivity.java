package com.example.examentipoaluisbaro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

private TextView lblNombre;
public EditText txtNombre;
private Button btnEntrar;
private Button btnSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                finish();
                System.exit(0);

            }
        });


        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cliente = txtNombre.getText().toString();

                if(cliente.matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar nombre",Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(MainActivity.this,Rectangulo.class);
                    intent.putExtra("cliente",cliente); ////Para enviar dato a otra actividad

                    startActivity(intent);
                }
            }
        });
    }
}
