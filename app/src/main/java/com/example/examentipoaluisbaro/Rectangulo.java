package com.example.examentipoaluisbaro;

import android.app.AppComponentFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Rectangulo extends AppCompatActivity {

    private RectanguloActivity RectanguloActivity;
    private TextView lblNombre;
    private TextView lblBase;
    private EditText txtBase;
    private TextView lblAltura;
    private EditText txtAltura;
    private TextView lblArea;
    private TextView lblCalArea;
    private TextView lblPerimetro;
    private TextView lblCalPerimetro;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        lblBase = (TextView) findViewById(R.id.lblBase);
        txtBase = (EditText) findViewById(R.id.txtBase);
        lblAltura = (TextView) findViewById(R.id.lblAltura);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        lblArea = (TextView) findViewById(R.id.lblArea);
        lblCalArea = (TextView) findViewById(R.id.lblCalArea);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);
        lblCalPerimetro = (TextView) findViewById(R.id.lblCalPerimetro);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        RectanguloActivity =  new RectanguloActivity();

        Bundle datos = getIntent().getExtras();
        String cliente = datos.getString("cliente");
        lblNombre.setText("Cliente: " + cliente);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtBase.getText().toString().matches("")){
                    Toast.makeText(Rectangulo.this,"Favor de llenar los campos",Toast.LENGTH_SHORT).show();
                }
                else if(txtAltura.getText().toString().matches("")){
                    Toast.makeText(Rectangulo.this,"Favor de llenar los campos",Toast.LENGTH_SHORT).show();
                }
                else{
                    String bas = txtBase.getText().toString();
                    float base = Float.parseFloat(bas);
                    String alt = txtAltura.getText().toString();
                    float altura = Float.parseFloat(alt);

                    RectanguloActivity.setBase(base);
                    RectanguloActivity.setAltura(altura);

                    String area = String.valueOf(RectanguloActivity.calcularArea());
                    lblCalArea.setText("Area: " + area);
                    String perimetro = String.valueOf(RectanguloActivity.calcularPerimetro());
                    lblCalPerimetro.setText("Perimetro: " + perimetro);
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                lblArea.setText("");
                lblPerimetro.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}